"""
Command line interface (CLI) test functions.
"""


import sys

import click
import flask

from hotpotato import tests


@click.group("test", cls=flask.cli.AppGroup)
def test():
    """
    Test commands.
    """
    pass


@test.command("create-data")
@click.option("--number", default=100, help="Number of each object to generate")
@click.option("--seed", default=None, help="Seed to use for the RNG")
def create_data(number, seed):
    """
    Fill the database with test data.
    """

    if flask.current_app.config["DEBUG"]:
        click.echo("Generating test data, please wait...")
        tests.create_data(num=number, seed=seed)
    else:
        click.echo("Not generating test data on a production system.")
        click.echo(
            "To allow generating test data, set Flask to debug mode or "
            'set "FLASK_DEBUG = true" in the config file.'
        )
        sys.exit(1)
