from unittest.mock import Mock

import requests
import twilio.rest
from requests.models import Response

from hotpotato.notifications import messages, send, senders
from hotpotato.tests import oncall_contacts, users


def modica_api_mock(url, data=None, json=None, **kwargs):
    """
    Return a successful modica response to outgoing requests
    """
    response = Mock(spec=Response)
    response.status_code = 201
    response.text = "[50]"
    return response


def test_send_modica_sms(monkeypatch, session):
    """
    Create and send a text message to an NZ user and check that
    the request is sent to the modica sender
    """
    oncall_user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        user=oncall_user, contact="+646458888555", method="sms"
    )
    session.commit()

    monkeypatch.setattr(requests, "post", modica_api_mock)

    notif = messages.create(
        tenant_id=None, body="This is a test message", to_user_id=oncall_user.id
    )
    send.using_sender(notif, senders.sms_modica.ModicaSMSSender(), contact)

    assert notif.has_been_sent()
    assert notif.json["provider"] == "modica"


def test_send_twilio_sms(monkeypatch, session):
    """
    Create and send a text message to a non NZ user and check that
    the request is sent to the twilio sender
    """
    oncall_user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        user=oncall_user, contact="+15888855555", method="sms"
    )
    session.commit()

    class TwilioMessageMock:
        sid = 50

    create = Mock(return_value=TwilioMessageMock())

    class TwilioMessagesMock:
        def __init__(self):
            self.create = create

    class TwilioClientMock:
        def __init__(self, sid, token):
            self.sid = sid
            self.token = token
            self.messages = TwilioMessagesMock()

    monkeypatch.setattr(twilio.rest, "Client", TwilioClientMock)

    notif = messages.create(
        tenant_id=None, body="This is a test message", to_user_id=oncall_user.id
    )
    send.using_sender(notif, senders.sms_twilio.TwilioSMSSender(), contact)

    assert create.call_count == 1

    assert notif.has_been_sent()
    assert notif.json["provider"] == "twilio"


def test_send_modica_pager(monkeypatch, session):
    """
    Create and send a text message to an NZ pager user and check that
    the request isin't sent to the modica sender
    """
    oncall_user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        user=oncall_user, contact="+646458888555", method="pager"
    )
    session.commit()

    # TODO check that the message is actually sent to the API

    monkeypatch.setattr(requests, "post", modica_api_mock)

    notif = messages.create(
        tenant_id=None, body="This is a test message", to_user_id=oncall_user.id
    )
    send.using_sender(notif, senders.pager_modica.ModicaPagerSender(), contact)

    assert notif.has_been_sent()
    assert notif.json["provider"] == "modica"
