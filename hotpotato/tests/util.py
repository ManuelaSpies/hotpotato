"""
Hot Potato testing module.
"""


import random


def get_random_object(session, model):
    """
    Get a random object from the given model.
    """

    count = int(session.query(model).count())
    return session.query(model).offset(int(count * random.random())).first()
