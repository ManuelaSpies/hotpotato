"""
Server functions.
"""


from datetime import datetime, timedelta

import pytz
import sqlalchemy

from hotpotato.models import Heartbeat, Server as Model, db


class ServerError(Exception):
    """
    Server exception base class.
    """

    pass


class ServerCreateError(Exception):
    """
    Server create exception class.
    """

    pass


class ServerIDError(ServerError):
    """
    Exception for invalid server ID.
    """

    def __init__(self, server_id):
        self.server_id = server_id
        super().__init__(
            "Invalid server ID (or unable to find server with ID): {}".format(server_id)
        )


class ServerAPIKeyError(ServerError):
    """
    Exception for invalid server API key.
    """

    def __init__(self, api_key):
        self.api_key = api_key
        super().__init__(
            "Invalid server API key (or unable to find server with API key): {}".format(
                api_key
            )
        )


class ServerTimezoneError(ServerError):
    """
    Exception for invalid server timezone.
    """

    def __init__(self, server_id, timezone):
        self.server_id = server_id
        self.timezone = timezone
        super().__init__(
            "Invalid timezone for server with ID {}: {}".format(
                self.server_id, self.timezone
            )
        )


def get(server_id):
    """
    Get a server object using the given ID and return it.
    """

    obj = Model.query.filter_by(id=server_id).first()

    if not obj:
        raise ServerIDError(server_id)

    return obj


def get_by(**kwargs):
    """
    Return a tuple containing all server object matching the given search parameters.
    """

    return Model.query.filter_by(**kwargs).all()


def get_by_api_key(api_key):
    """
    Get a server object using its API key and return it.
    """

    obj = Model.query.filter_by(apikey=api_key).first()

    if not obj:
        raise ServerAPIKeyError(api_key)

    return obj


def missed_hbeats_query(now, hbeat_freq):
    """
    Return an SQL query object for finding servers have not sent heartbeats
    within their threshold time, or have never sent a heartbeat.
    """

    # Subquery which is use to select the last heartbeat for each server,
    # so it can be used in a filter operation in the main query.
    subquery_hbeats = (
        db.session.query(
            Heartbeat.server_id,
            sqlalchemy.func.max(Heartbeat.received_dt).label("last_hbeat"),
        )
        .group_by(Heartbeat.server_id)
        .subquery()
    )

    return (
        db.session.query(
            # Select server ID, hostname, missed heartbeat limit and the last heartbeat.
            Model.id,
            Model.hostname,
            Model.missed_heartbeat_limit,
            subquery_hbeats.c.last_hbeat,
        )
        .outerjoin(
            # Outer join with the heartbeats subquery to get the last heartbeat result.
            subquery_hbeats,
            subquery_hbeats.c.server_id == Model.id,
        )
        .filter(
            # Only select rows if:
            # * heartbeats are enabled
            # * missed heartbeat notifications are turned on
            # * their last heartbeat exceeds the threshold time,
            #   OR there have been no heartbeats at all
            Model.enable_heartbeat == sqlalchemy.sql.expression.true(),
            Model.disable_missed_heartbeat_notif == sqlalchemy.sql.expression.false(),
            sqlalchemy.or_(
                sqlalchemy.sql.expression.cast(
                    now - subquery_hbeats.c.last_hbeat, sqlalchemy.types.Integer
                )
                >= Model.missed_heartbeat_limit * hbeat_freq,
                subquery_hbeats.c.server_id.is_(None),
            ),
        )
        .group_by(
            # Neatly group the results by ID.
            Model.id,
            Model.hostname,
            Model.missed_heartbeat_limit,
            subquery_hbeats.c.last_hbeat,
        )
    )


def get_num_missed_hbeats():
    """
    Return the number of servers which have not sent heartbeats within their threshold time,
    or have never sent a heartbeat.
    """

    now = datetime.utcnow()

    # Heartbeat frequency, in seconds.
    # Ideally should be configurable, and the function is designed around this.
    hbeat_freq = 60

    return missed_hbeats_query(now, hbeat_freq).count()


def get_missed_hbeats():
    """
    Return a dict of servers which have not sent heartbeats within their threshold time,
    or have never sent a heartbeat.

    Format:
        missed_servs[id] = {        # The server ID in the database.
            hostname,               # The hostname of the server.
            missed_heartbeat_limit, # The threshold number of heartbeats to miss before paging.
            hbeat_threshold,        # The threshold time (in seconds) to page for
                                      failing to receive heartbeats.
            last_hbeat,             # The timestamp of the last heartbeat received. Can be None.
            last_hbeat_ago,         # The time between the last heartbeat timestamp and now.
                                      Can be None.
        }
    """

    now = datetime.utcnow()

    # Heartbeat frequency, in seconds.
    # Ideally should be configurable, and the function is designed around this.
    hbeat_freq = 60

    missed_servs_res = missed_hbeats_query(now, hbeat_freq).all()

    # Convert the array of tuples into a dict.
    missed_servs = {
        serv[0]: {
            "hostname": serv[1],
            "missed_heartbeat_limit": serv[2],
            "hbeat_threshold": timedelta(seconds=serv[2] * hbeat_freq),
            "last_hbeat": serv[3],
            "last_hbeat_ago": now - serv[3] if serv[3] is not None else None,
        }
        for serv in missed_servs_res
    }

    return missed_servs


def get_monitored():
    """
    Get a list of all the servers that are currently being actively monitored.
    Does not list servers whose notifications have temporarily been disabled, therefore
    it does not constitute a "list of servers that we care about on an on-going basis".
    """
    return Model.query.filter_by(
        enable_heartbeat=True, disable_missed_heartbeat_notif=False
    ).all()


# pylint: disable=too-many-arguments
def create(
    hostname,
    apikey,
    timezone,
    link,
    enable_heartbeat=True,
    disable_missed_heartbeat_notif=False,
    missed_heartbeat_limit=5,
):
    """
    Create a new server,  and add it to the database, and return it
    """

    if get_by(hostname=hostname):  # TODO: tenants - make this match tenant as well
        raise ServerCreateError(
            'Server with hostname "{}" already exists'.format(hostname)
        )

    if get_by(apikey=apikey):
        raise ServerCreateError(
            'Server with API key "{}" already exists'.format(apikey)
        )

    server = Model(
        hostname=hostname,
        apikey=apikey,
        enable_heartbeat=enable_heartbeat,
        disable_missed_heartbeat_notif=disable_missed_heartbeat_notif,
        missed_heartbeat_limit=missed_heartbeat_limit,
        timezone=timezone,
        link=link,
    )

    db.session.add(server)
    db.session.commit()

    return server


def timezone_get_as_timezone(tz_str):
    """
    Return the given timezone string as a timezone object.
    """

    return pytz.timezone(tz_str)
