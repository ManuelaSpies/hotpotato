"""
Role functions.
"""


from hotpotato.models import Role as Model, db, user_datastore


class RoleError(Exception):
    """
    Role exception base class.
    """

    pass


class RoleIDError(RoleError):
    """
    Exception for invalid role ID.
    """

    def __init__(self, role_id):
        self.role_id = role_id
        super().__init__(
            "Invalid role ID (or unable to find role with ID): {}".format(role_id)
        )


class RoleNameError(RoleError):
    """
    Exception for invalid role name.
    """

    def __init__(self, name):
        self.name = name
        super().__init__(
            "Invalid role name (or unable to find role with name): {}".format(name)
        )


def get(role_id):
    """
    Get a role object using the given ID, and return it.
    """

    role = Model.query.filter_by(id=role_id).first()

    if not role:
        raise RoleIDError(role_id)

    return role


def get_by_name(name):
    """
    Get a role object using the given name, and return it.
    """

    role = Model.query.filter_by(name=name).first()

    if not role:
        raise RoleNameError(name)

    return role


def get_by(**kwargs):
    """
    Return a list of role objects which match the given search parameters.
    """

    return Model.query.filter_by(**kwargs).all()


def create(name, description):
    """
    Create a role object using the given input data, add it to the database,
    and return it.
    """

    role = user_datastore.create_role(name=name, description=description)
    db.session.commit()

    return role
