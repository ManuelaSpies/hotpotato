"""
Notification Pushover sender classes and functions.
"""


import flask
import requests

from hotpotato.notifications import exceptions
from hotpotato.notifications.senders.sms import SMSBaseSender


class PushoverSender(SMSBaseSender):
    """
    Notification Pushover sender.
    """

    name = "pushover"
    description = "Pushover"
    method = "pushover"
    provider = "pushover"

    def send(self, notif, contact):
        """
        Send a notification to an individual contact via Pushover
        """

        success = False
        warnings = []
        errors = []
        provider_notif_id = None

        token = flask.current_app.config["PUSHOVER_API_TOKEN"]

        priority = 0
        title = "Hot Potato"

        if notif.notif_type == "alert":
            priority = flask.current_app.config["PUSHOVER_ALERT_PRIORITY"]
            title = "Hot Potato Alert"
        elif notif.notif_type == "handover":
            priority = flask.current_app.config["PUSHOVER_HANDOVER_PRIORITY"]
            title = "Hot Potato Handover"
        elif notif.notif_type == "message":
            priority = flask.current_app.config["PUSHOVER_MESSAGE_PRIORITY"]
            title = "Hot Potato Message"

        s = requests.Session()

        limits = s.get(
            "https://api.pushover.net/1/apps/limits.json?token={}".format(token)
        )

        if limits.status_code != 200:
            raise exceptions.NotificationSendError(
                "Error got invalid response from pushover limits API"
            )

        if limits.json()["remaining"] == 0:
            raise exceptions.NotificationSendError(
                "Pushover send quota has been exceeded"
            )

        message_json = {
            "token": token,
            "user": contact.contact,
            "title": title,
            "message": notif.body,
            "priority": priority,
            "timestamp": notif.received_dt.timestamp(),
            "url": flask.url_for("notifications_get_by_id", notif_id=notif.id),
            "url_title": "Link to notification",
        }
        if priority == 2:
            message_json["retry"] = flask.current_app.config["PUSHOVER_EMERGENCY_RETRY"]
            message_json["expire"] = flask.current_app.config[
                "PUSHOVER_EMERGENCY_EXPIRY"
            ]
            message_json["callback"] = flask.url_for(
                "api_server_v2.senders_pushover_notificationstatus"
            )

        response = s.post("https://api.pushover.net/1/messages.json", json=message_json)

        if response.status_code != 200 or response.json()["status"] != 1:
            raise exceptions.NotificationSendError(
                "Error {}: {}".format(response.status_code, response.json()["errors"])
            )

        # If we got this far, the operation succeeded and we can return.
        success = True
        provider_notif_id = None
        if priority == 2:
            provider_notif_id = response.json()["receipt"]

        return {
            "success": success,
            "warnings": warnings,
            "errors": errors,
            "provider_notif_id": provider_notif_id,
        }

    def can_send(self, user_id):
        """
        Return True if the user with the given ID can use the Pushover sender,
        and sending via Pushover is enabled.
        """

        return (
            super().can_send(user_id)
            if flask.current_app.config["PUSHOVER_ENABLED"]
            else False
        )
