"""
Notification SMS sender base class.
"""


import flask

from hotpotato.notifications.senders.base import BaseSender


# pylint: disable=abstract-method
class SMSBaseSender(BaseSender):
    """
    Notification SMS sender base class.
    """

    method = "sms"

    def can_send(self, user_id):
        """
        Return True if the user with the given ID can use the SMS sender,
        and sending via SMS is enabled.
        """

        return (
            super().can_send(user_id)
            if flask.current_app.config["SMS_ENABLED"]
            else False
        )
