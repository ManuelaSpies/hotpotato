"""
Notification Modica sender base class.
"""


import re

import requests
from flask import current_app

from hotpotato.notifications import exceptions
from hotpotato.notifications.senders.base import BaseSender


# pylint: disable=abstract-method
class ModicaBaseSender(BaseSender):
    """
    Notification Modica sender base class.
    """

    provider = "modica"

    def send(self, notif, contact):
        """
        Send a notification to an individial contact via Modica.
        """

        success = False
        warnings = []
        errors = []
        provider_notif_id = None

        api_url = current_app.config["MODICA_URL"]
        api_user = current_app.config["MODICA_USERNAME"]
        api_pass = current_app.config["MODICA_PASSWORD"]

        # Check if the contact number starts with a +64 first.
        if not contact.contact.startswith("+64"):
            raise ValueError(
                "Only New Zealand (+64) phone numbers are supported by Modica"
            )

        # Send the notification.
        response = requests.post(
            "{}/{}".format(api_url, "messages"),
            auth=(api_user, api_pass),
            timeout=5,
            json={"destination": contact.contact, "content": notif.body},
        )

        if response.status_code != 201:
            raise exceptions.NotificationSendError(
                "Error {}: {}".format(response.status_code, response.json()["error"])
            )

        # Success! Get the provider notification ID,
        # removing the [] that Modica encloses the ID in.
        success = True
        # provider_notif_id is supposed to be a string, even if Modica's output is an integer.
        provider_notif_id = re.sub(r"[\[\]]", "", response.text)

        return {
            "success": success,
            "warnings": warnings,
            "errors": errors,
            "provider_notif_id": provider_notif_id,
        }
