"""
Handover notification classes and functions.
"""


from hotpotato.models import Handover, User
from hotpotato.notifications import notifications

NOTIF_TYPE = "handover"

# A frozen set of values representing all possible JSON API parameters for a handover notification.
JSON_API_PARAMS = frozenset(("rotation_id", "to_user_id", "from_user_id", "message"))


def get(notif_id):
    """
    Get a handover.
    """

    return notifications.get(notif_id)


def get_by(**kwargs):
    """
    Return a tuple of all handover notifications matching the given search parameters.
    """

    kwargs["notif_type"] = "handover"
    return notifications.get_by_helper(**kwargs)


def create(tenant_id, rotation_id, to_user_id, from_user_id=None, message=None):
    """
    Create a new pair of handover notifications, and returns it as a tuple,
    with the first being the handover notifications for the new on-call person,
    and the second being the handover notifications for the current on-call person.

    If from_user_id is not specified the handover notifications for the current on-call
    person will not be created, and the second element of the tuple will be None.
    """

    to_user = User.query.filter_by(id=to_user_id).first()
    from_user = User.query.filter_by(id=from_user_id).first()

    if from_user and message:
        body = "{} is now on call, taking over from {} with the following message: {}".format(
            to_user.name, from_user.name, message
        )
    elif from_user:
        body = "{} is now on call, taking over from {}.".format(
            to_user.name, from_user.name
        )
    elif message:
        body = "{} is now on call with the following message: {}".format(
            to_user.name, message
        )
    else:
        body = "{} is now on call.".format(to_user.name)

    json_obj = {
        "rotation_id": rotation_id,
        "to_user_id": to_user.id,
        "from_user_id": from_user.id if from_user else None,
        "message": message,
    }

    return (
        notifications.create(
            tenant_id=tenant_id,
            notif_model=Handover,
            body=body,
            user_id=to_user.id,
            json_obj=json_obj,
        ),
        notifications.create(
            tenant_id=tenant_id,
            notif_model=Handover,
            body=body,
            user_id=from_user_id,
            json_obj=json_obj,
        )
        if from_user_id
        else None,
    )
