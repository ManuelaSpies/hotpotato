"""
API exception classes.
"""


class APIError(Exception):
    """
    API exception base class.
    """

    def __init__(self, message, success, code):
        """
        API exception base class constructor.
        """

        self.code = code
        self.success = success
        super().__init__(message)
