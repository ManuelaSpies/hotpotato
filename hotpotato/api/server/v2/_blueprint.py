"""
Server API version 2 blueprint.
"""


import flask

blueprint = flask.Blueprint("api_server_v2", __name__)
