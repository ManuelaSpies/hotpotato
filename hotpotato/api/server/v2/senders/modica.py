"""
Server API version 2 endpoints for Modica callback functions.
"""


from hotpotato.api.server import modica
from hotpotato.api.server.v2._blueprint import blueprint


@blueprint.route("/senders/modica/mo-message", methods=["POST"])
def senders_modica_mo_message():
    """
    Handle incoming SMS messages from Modica.
    """

    return modica.mo_message()


@blueprint.route("/senders/modica/dlr-status", methods=["POST"])
def senders_modica_dlr_status():
    """
    Handle delivery status receipts from Modica.
    """

    return modica.notificationstatus()
