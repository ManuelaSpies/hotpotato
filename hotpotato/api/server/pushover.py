"""
Server API endpoints for Pushover callbacks.
"""


import flask
from flask import current_app
from webargs import fields
from webargs.flaskparser import parser

from hotpotato import models
from hotpotato.notifications import (
    exceptions as notifications_exceptions,
    notifications,
)


class ProviderNotificationStatusError(Exception):
    """
    Message status exception class.
    """

    pass


notif_args = {
    "receipt": fields.Str(required=True),
    "acknowledged": fields.Int(required=True),
}


def notificationstatus():
    """
    Handle delivery status receipts from Pushover.
    """

    json_obj = parser.parse(notif_args, flask.request)

    provider_notif_id = json_obj["receipt"]
    provider_notif_status = json_obj["acknowledged"]

    current_app.logger.debug("Received acknowledgement receipt from Pushover:")
    current_app.logger.debug("- provider_notif_id = {}".format(provider_notif_id))
    current_app.logger.debug(
        "- provider_notif_status = {}".format(provider_notif_status)
    )

    # Try to find the notification associated with the provider notification ID.
    try:
        notif = notifications.get_by_provider_notif_id("pushover", provider_notif_id)
    except notifications_exceptions.NotificationProviderNotificationIDError as err:
        err_message = "While receiving message status from Pushover: {}".format(
            str(err)
        )
        current_app.logger.error(err_message)
        return (err_message, 400)

    current_app.logger.debug(
        "Found associated notification with ID: {}".format(notif.id)
    )

    # Modify the status of the message based on the information in the acknowledgement receipt.
    if provider_notif_status == 1:
        notif.json["status"] = "READ_BY_CLIENT"
    else:
        body = "Unknown message status value received from Pushover: {}".format(
            provider_notif_status
        )
        notif.json["errors"].append(body)
        raise ProviderNotificationStatusError(body)

    current_app.logger.debug(
        "Changed status of notification with ID {} to {}".format(
            notif.id, notif.json["status"]
        )
    )

    if notif.json["status"] == "SEND_FAILED":
        current_app.logger.error(
            "Failed to send notification with ID {}: {}".format(
                notif.id, notif.json["errors"][-1]
            )
        )

    models.db.session.commit()

    return ("", 204)
