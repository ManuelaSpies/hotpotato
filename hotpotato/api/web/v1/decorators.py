"""
Web API version 1 helper decorators.
"""

from flask_security import login_required

__all__ = ["login_required"]  # TODO: add auth_required to this


# TODO: Add API keys for users, and use a custom decorator to add support for that
#       AND regular logins
# def auth_required(func):
#     '''
#     Decorator for making sure that an API endpoint can only be accessed if the user
#     is authenticated, either being logged in OR using an API key.
#     NOTE: This needs to be BELOW (inside) the Flask route decorator AND the @endpoint
#           decorator.
#     '''
#
#     # pylint: disable=missing-docstring
#     @functools.wraps(func)
#     def inner(*args, **kwargs):
#         try:
#             functions.server_get_from_auth()
#             return func(*args, **kwargs)
#         except exceptions.AuthError as err:
#             return functions.response_get(success=False, code=401, message=str(err))
#
#     return inner
