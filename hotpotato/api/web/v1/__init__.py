"""
Web API version 1 blueprint and endpoints.
"""


from hotpotato.api.web.v1 import errors  # noqa: F401
from hotpotato.api.web.v1 import notifications  # noqa: F401
from hotpotato.api.web.v1._blueprint import blueprint  # noqa: F401
